package com.example.amartineza.practicalistas;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amartineza on 2/27/2018.
 */

public class ListViewClass extends AppCompatActivity {

    private ListView lvMascotas;
    private PetListViewAdapter petListViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);

        lvMascotas = findViewById(R.id.pet_list);

        List<ModeloMascota> list = new ArrayList<>();

        for (int i = 0; i < 15; i++) {
            if(i%2==0){
                ModeloMascota model = new ModeloMascota("Lucas", "Adriana Martinez", "Macho", "gato");
                list.add(model);
            }else{
                ModeloMascota model = new ModeloMascota("Lucas", "Adriana Martinez", "Macho", "perro");
                list.add(model);
            }
        }

        petListViewAdapter = new PetListViewAdapter(ListViewClass.this, list);
        lvMascotas.setAdapter(petListViewAdapter);
        lvMascotas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ModeloMascota model = petListViewAdapter.geModel(i);
                Toast.makeText(ListViewClass.this, model.getNombreDueño(), Toast.LENGTH_LONG).show();
            }
        });


    }

}
