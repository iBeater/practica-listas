package com.example.amartineza.practicalistas;

/**
 * Created by amartineza on 2/26/2018.
 */

public class ModeloMascota {
    private String nombreMascota;
    private String nombreDueño;
    private String sexoMascota;
    private String tipoMascota;

    public ModeloMascota(String nombreMascota, String nombreDueño, String sexoMascota, String tipoMascota) {
        this.nombreMascota = nombreMascota;
        this.nombreDueño = nombreDueño;
        this.sexoMascota = sexoMascota;
        this.tipoMascota = tipoMascota;

    }

    public String getNombreMascota() {
        return nombreMascota;
    }

    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota;
    }

    public String getNombreDueño() {
        return nombreDueño;
    }

    public void setNombreDueño(String nombreDueño) {
        this.nombreDueño = nombreDueño;
    }

    public String getSexoMascota() {
        return sexoMascota;
    }

    public void setSexoMascota(String sexoMascota) {
        this.sexoMascota = sexoMascota;
    }

    public String getTipoMascota() {
        return tipoMascota;
    }

    public void setTipoMascota(String tipoMascota) {
        this.tipoMascota = tipoMascota;
    }
}
