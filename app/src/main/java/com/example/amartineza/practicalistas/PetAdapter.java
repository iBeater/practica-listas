package com.example.amartineza.practicalistas;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amartineza on 2/26/2018.
 */

public class PetAdapter extends RecyclerView.Adapter<PetAdapter.ItemViewHolder> {
    private Context mContexto;
    private List<ModeloMascota> mList = new ArrayList<>();
    private onItemClicked mListener;

    public PetAdapter(Context mContexto, List<ModeloMascota> mList, onItemClicked mListener) {
        this.mContexto = mContexto;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mascota, parent, false);

        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        final ModeloMascota model = mList.get(position);

        holder.tvDueño.setText(model.getNombreDueño());
        holder.tvNombreMascota.setText(model.getNombreMascota());
        holder.tvSexoMascota.setText(model.getSexoMascota());
        if(model.getTipoMascota()=="gato"){
            holder.ivMascota.setImageResource(R.drawable.gato);
        }else{
            holder.ivMascota.setImageResource(R.drawable.perro);
        }

       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               mListener.onPetListener(model);
           }
       });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        ImageView ivMascota;
        TextView tvDueño;
        TextView tvNombreMascota;
        TextView tvSexoMascota;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ivMascota = itemView.findViewById(R.id.foto_mascota);
            tvDueño = itemView.findViewById(R.id.dueño);
            tvNombreMascota = itemView.findViewById(R.id.nombre_mascota);
            tvSexoMascota = itemView.findViewById(R.id.sexo_mascota);
        }
    }

    public interface onItemClicked{
        void onPetListener(ModeloMascota model);
    }

}
