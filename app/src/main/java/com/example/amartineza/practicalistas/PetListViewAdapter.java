package com.example.amartineza.practicalistas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amartineza on 2/26/2018.
 */

public class PetListViewAdapter  extends BaseAdapter {

    private Context mContext;
    private List<ModeloMascota> mlist = new ArrayList<>();

    public PetListViewAdapter(Context mContext, List<ModeloMascota> mlist) {
        this.mContext = mContext;
        this.mlist = mlist;
    }

    @Override
    public int getCount() {
        return mlist.size(); //retonar n elementos
    }

    public ModeloMascota geModel(int position){
        return mlist.get(position);
    }

    @Override
    public Object getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        ModeloMascota model = mlist.get(position);

        if(convertView== null){
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_mascota, parent, false);
        }

        ImageView ivMascota = view.findViewById(R.id.foto_mascota);
        TextView tvMascota = view.findViewById(R.id.nombre_mascota);
        TextView tvDueño = view.findViewById(R.id.dueño);
        TextView tvSexo = view.findViewById(R.id.sexo_mascota);

        tvDueño.setText(model.getNombreDueño());
        tvMascota.setText(model.getNombreMascota());
        tvSexo.setText(model.getSexoMascota());

        if(model.getTipoMascota()=="gato"){
            ivMascota.setImageResource(R.drawable.gato);
        }else{
            ivMascota.setImageResource(R.drawable.perro);
        }



        return view;
    }
}
