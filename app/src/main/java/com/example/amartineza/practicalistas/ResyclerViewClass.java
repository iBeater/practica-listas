package com.example.amartineza.practicalistas;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amartineza on 2/27/2018.
 */

public class ResyclerViewClass extends AppCompatActivity  implements PetAdapter.onItemClicked{

    private RecyclerView rvMascotas;
    private PetAdapter adapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view);


        setContentView(R.layout.recycler_view);
        rvMascotas = findViewById(R.id.rv_mascota);

        List<ModeloMascota> list = new ArrayList<>();

        for (int i = 0; i < 15; i++) {
            if(i%2==0){
                ModeloMascota model = new ModeloMascota("Lucas", "Adriana Martinez", "Macho", "gato");
                list.add(model);
            }else{
                ModeloMascota model = new ModeloMascota("Lucas", "Adriana Martinez", "Macho", "perro");
                list.add(model);
            }
        }

        adapter = new PetAdapter(ResyclerViewClass.this, list, this);
        rvMascotas.setAdapter(adapter);
        rvMascotas.setLayoutManager(new LinearLayoutManager(ResyclerViewClass.this, LinearLayoutManager.VERTICAL, false));
        rvMascotas.hasFixedSize(); //siempre tienen el mismo tamaño



        adapter = new PetAdapter(ResyclerViewClass.this, list, this);
        rvMascotas.setAdapter(adapter);
        rvMascotas.setLayoutManager(new LinearLayoutManager(ResyclerViewClass.this, LinearLayoutManager.VERTICAL, false));
        rvMascotas.hasFixedSize(); //siempre tienen el mismo tamaño
    }

    @Override
    public void onPetListener(ModeloMascota model) {
        Toast.makeText(ResyclerViewClass.this, "Jeje", Toast.LENGTH_SHORT).show();
    }

}
